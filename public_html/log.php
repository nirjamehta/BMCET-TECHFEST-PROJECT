<?php

/*
 * Copyright (C) 2018 VanGiex
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require '../vendor/autoload.php';

// load Monolog library
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Processor\WebProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Formatter\JsonFormatter;

// create a log channel
$log = new Logger('AAYAM 2018');

// create a Json formatter
$formatter = new JsonFormatter();

// create a handler
$stream = new StreamHandler('../Logs/application-json.log', Logger::DEBUG);
$stream->setFormatter($formatter);

// bind
$log->pushHandler($stream);
// processor, adding URI, IP address etc. to the log
$log->pushProcessor(new WebProcessor);
// processor, memory usage
$log->pushProcessor(new MemoryUsageProcessor);

// Log
$log->info('Navigation', array(json_encode($_GET),json_encode($_POST),json_encode($_REQUEST),json_encode($_SESSION),json_encode($_COOKIE),json_encode($_SERVER)));


if ( $_GET['GET'] == "rules")
{
    header("Location: doc/rules.pdf");
    exit();
}