<!-- 
/* 
 * Copyright (C) 2018 VanGiex
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
-->
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="">
        <!-- Meta Description -->
        <meta name="description" content="AAYAM IS THE OFFICAL TECHFEST OF BMEF SURAT GUJARAT INDIA">
        <!-- Meta Keyword -->
        <meta name="keywords" content="BMEF TECHFEST">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>AAYAM | BMEF</title>

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
        <!-- ================================================ CSS ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="css/availability-calendar.css" rel="stylesheet">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/baguetteBox.min.css">
        <link rel="stylesheet" href="css/gallery-grid.css">
    </head>
    <body>
        
        <?php
         require 'log.php';
         ?>

        <!-- Start Header Area -->
        <header class="default-header">
            <div class="container">
                <div class="header-wrap">
                    <div class="header-top d-flex justify-content-between align-items-center">
                        <div class="logo">
                            <a href="#home"><img src="img/logo.png" alt=""></a>
                        </div>
                        <div class="main-menubar d-flex align-items-center">
                            <nav class="hide">
                                <a href="index.php#home">Home</a>
                                <a href="index.php#aayam">AAYAM</a>
                                <a href="index.php#events">EVENTS</a>
                                <a href="enrollment.php">Enrollments</a>
                                <a href="index.php#events">Particepate</a>
                                <a href="index.php#about">ABOUT US</a>
                            </nav>
                            <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- End Header Area -->

        <!-- start banner Area -->
        <section class="banner-area relative" id="home">
            <div class="overlay overlay-bg"></div>
            <div class="container">
                <div class="row fullscreen align-items-center justify-content-center" style="height: 915px;">
                    <div class="banner-content col-lg-6 col-md-12">
                        <h1>
                            Our Event Starts in
                        </h1>
                        <div class="row clock_sec d-flex flex-row justify-content-between" id="clockdiv">
                            <div class="clockinner">
                                <span class="days"></span>
                                <div class="smalltext">Days</div>
                            </div>
                            <div class="clockinner">
                                <span class="hours"></span>
                                <div class="smalltext">Hours</div>
                            </div>
                            <div class="clockinner">
                                <span class="minutes"></span>
                                <div class="smalltext">Minutes</div>
                            </div>
                            <div class="clockinner">
                                <span class="seconds"></span>
                                <div class="smalltext">Seconds</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End banner Area -->

        <!-- Start facilities Area -->
        <section id="aayam" class="facilities-area section-gap">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-8 pb-80 header-text">
                        <h1>What Is Aayam</h1>
                        <p style="text-align: justify">
                            Aayam! It is annual technical festival of BMEF. It also refers to the independent body of students who organise their event along with many other outreach programs around. 

                            Aayam is known for hosting a variety of events that include competition, exhibitions, seminars as well as workshops.

                            Started in 2013 with the aim of providing a platform for students to develop and showcase their technical power and to grow in the real world of technology 
                        </p>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-md-8 pb-80 header-text">
                        <h1>Who Organise Aayam</h1>
                        <p style="text-align: justify">
                            BMEF is one of the premier education foundation in surat, offering various courses from different college which includes all courses,
                            Engineering	Pharmacy,BBA/MBA,Architecture,Polytechnic,Nursing,BCA,MCA,Hotel ManagementWe firmly believe that education must not be limited purely to academic learning. The BMEF campus is not just about academics but offers a transformative experience. The campus also provides learning spaces with advanced audio, video, conferencing and networking facilities.
                            In BMEF, students from diverse backgrounds are encouraged to interact with each other over  various activities, which each other over various activities, which are aimed to bring BMEF community together. 
                            Sports like cricket, soccer, basketball, badminton, and other athletics; Extra-curricular activities ranging from theatre, music, art, quizzing, debates and more, provide students with avenues to express their talents and to develop new interests, which will last for a lifetime. 


                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- End facilities Area -->
        <section class="content-image">
            <div style="background-color:#F9F9FF" class="container-fluid gallery-container">
                <br>
                <h1>BMEF Gallery</h1>

                <p  class="page-description text-center">Seeing old photographs, we get enamored by the memories we made which will keep tugging at our heart-strings forever and ever...</p>

                <div class="tz-gallery">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="img/bg1.jpg">
                                <img src="img/bg1.jpg" alt="Park">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="img/bg2.jpg">
                                <img src="img/bg2.jpg" alt="Bridge">
                            </a>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <a class="lightbox" href="img/bg3.jpg">
                                <img src="img/bg3.jpg" alt="Tunnel">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="img/bg4.jpg">
                                <img src="img/bg4.jpg" alt="Coast">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="img/bg5.jpg">
                                <img src="img/bg5.jpg" alt="Rails">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="img/bg6.jpg">
                                <img src="img/bg6.jpg" alt="Traffic">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="img/bg7.jpg">
                                <img src="img/bg7.jpg" alt="Rocks">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="img/bg8.jpg">
                                <img src="img/bg8.jpg" alt="Benches">
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="img/bg9.jpg">
                                <img src="img/bg9.jpg" alt="Sky">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Start calender Area -->
        <section  class="calender-area relative section-gap">
            <div class="overlay overlay-bg"></div>
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-8 pb-80 header-text">
                        <h1>Calendar</h1>
                        <p>
                            You should run your life not by the calendar but how you feel <br> what you're interests are and ambitions 
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 calender-wrap" id="calendar"></div>
                    <div class="col-lg-12 event-dates">
                        <div class="single-event d-flex flex-row">
                            <p class="col">
                                Feb 15
                            </p>
                            <p class="col-8">
                                Event campaign
                            </p>
                            <p class="col text-right">
                                <span class="lnr lnr-highlight"></span>

                            </p>
                        </div>
                        <div class="single-event d-flex flex-row">
                            <p class="col">
                                Feb 16
                            </p>
                            <p class="col-8">
                                Registration Drive
                            </p>
                            <p class="col text-right">
                                <span class="lnr lnr-highlight"></span>

                            </p>
                        </div>
                        <div class="single-event d-flex flex-row">
                            <p class="col">
                                Mar 6
                            </p>
                            <p class="col-8">
                                Event Day 1
                            </p>
                            <p class="col text-right">
                                <span class="lnr lnr-highlight"></span>

                            </p>
                        </div>
                        <div class="single-event d-flex flex-row">
                            <p class="col">
                                Mar 7
                            </p>
                            <p class="col-8">
                                Event Day 2
                            </p>
                            <p class="col text-right">
                                <span class="lnr lnr-highlight"></span>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End calender Area -->


        <!-- Start events Area -->
        <section id="events" class="events-area section-gap">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-8 pb-80 header-text">
                        <h1>Upcoming Events</h1>
                        <p>
                            It is very rare or impossible that an event can be negative from all points of view.
                        </p>
                    </div>
                </div>
                <div class="row no-padding">
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e1.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Switch Maze">
                                    <h4>Switch Maze</h4>
                                </a>
                                <p>
                                    Planet of quality, place of pride it's time to connect circuit right. Price=100/2 Members
                                </p>
                                <p class="meta">
                                    <span class="lnr lnr-heart"></span> 
                                    <span class="likes" ><a href="enroll.php?set=Switch Maze">Register</a></span>
                                    <span class="lnr lnr-bubble"></span> 
                                    <span class="likes"><a href="enquiry.php">enquiry</a></span>
                                    <span class="lnr lnr-database"></span> 
                                    <span class="likes"><a href="log.php?GET=rules">Rules</a></span>
                                </p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e2.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Logo Designing">
                                    <h4>Logo Designing</h4>
                                </a>
                                <p>
                                    Creating at foundation for learning. Price=50/2 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Logo Designing"">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e22.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Dam O Mania">
                                    <h4>Dam O Mania</h4>
                                </a>
                                <p>
                                    Let's water show its strength to your concrete. Price=100/3 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Dam O Mania">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e3.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=NFS GAMING">
                                    <h4>NFS GAMING</h4>
                                </a>
                                <p>
                                    Fire with the art of speed, respect the art of lives. Price=50/1 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=NFS GAMING">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e4.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Pirate BATTLE">
                                    <h4>Pirate BATTLE</h4>
                                </a>
                                <p>
                                    Sail your ship and counting weight. Price=60/4 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Pirate BATTLE">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e5.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Counter Strike GAMING">
                                    <h4>Counter Strike GAMING</h4>
                                </a>
                                <p>
                                    Just start firing and you will know what it feels. Price=120/5 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Counter Strike GAMING">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e6.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=ROBO Race">
                                    <h4>ROBO Race</h4>
                                </a>
                                <p>
                                    Let's face the rights and lefts of surat city with your super fast robots. Price=200/4 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=ROBO Race">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e7.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Mini Militia">
                                    <h4>Mini Militia</h4>
                                </a>
                                <p>
                                    It's not a big deal, it's a real deal. Price=60/2 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Mini Militia">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e8.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Short Film">
                                    <h4>Short Film</h4>
                                </a>
                                <p>
                                    Creating moments of magic and colour. Price=50/3 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Short Film">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e9.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Model Presentation">
                                    <h4>Model Presentation</h4>
                                </a>
                                <p>
                                    Encircling the photorealistic world of 3D. Price=50/3 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Model Presentation">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e10.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Town O Planning">
                                    <h4>Town O Planning</h4>
                                </a>
                                <p>
                                    Cementing your dams on bricks with measurements. Price=120/4 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Town O Planning">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e11.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Bascule Bridge">
                                    <h4>Bascule Bridge</h4>
                                </a>
                                <p>
                                    See the counted weight continuously balances. Price=80/4 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Bascule Bridge">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e12.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=ROBO Soccer">
                                    <h4>ROBO Soccer</h4>
                                </a>
                                <p>
                                    The competition area where you will have special place to define your robot's power. Price=200/4 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=ROBO Soccer">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e13.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Auto Sketching">
                                    <h4>Auto Sketching</h4>
                                </a>
                                <p>
                                    Interdimensional of your faster wheels.Price=50/1 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Auto Sketching">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e14.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Circuitronix">
                                    <h4>Circuitronix</h4>
                                </a>
                                <p>
                                    Achievement a balance between series and parallel and their components. Price=50/2 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Circuitronix">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e15.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Techno Treasure Hunt">
                                    <h4>Techno Treasure Hunt</h4>
                                </a>
                                <p>
                                    Run to concrete, cement and brick to gain your pride. Price=80/4 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Techno Treasure Hunt">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e16.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Poster Presentation">
                                    <h4>Poster Presentation</h4>
                                </a>
                                <p>
                                    Creativity tools for today and tomorrow. Price=50/3 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Poster Presentation">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-sm-6">
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e17.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Relay Coding">
                                    <h4>Relay Coding</h4>
                                </a>
                                <p>
                                    It's not simply the JAVA in your up . it's how you prepare. Price=50/2 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Relay Coding">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e18.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=ROBO Tug Of War">
                                    <h4>ROBO Tug Of War</h4>
                                </a>
                                <p>
                                    Teams against each others to test the strength of one's robot. Price=150/4 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=ROBO Tug Of War">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;" src="img/e19.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Cade Zap">
                                    <h4>Cade Zap</h4>
                                </a>
                                <p>
                                    Come design your imagination with dimension. Price=50/1 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Cade Zap">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e20.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Code War">
                                    <h4>Code War</h4>
                                </a>
                                <p>
                                    All we need is some codes and languages. Price=50/2 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Code War">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                        <div class="single-events row no-padding">
                            <div class="col-lg-4 image">
                                <img style="height:195px;width:100%;"  src="img/e21.jpg" alt="">
                            </div>
                            <div class="col-lg-7 details">
                                <a href="enroll.php?set=Eurepa">
                                    <h4>Eurepa</h4>
                                </a>
                                <p>
                                    Let's reach the height with the support of rope. Price=200/4 Members
                                </p>
                                <p class="meta"><span class="lnr lnr-heart"></span> <span class="likes" ><a href="enroll.php?set=Eurepa">Register</a></span> <span class="lnr lnr-bubble"></span> <span class="likes"><a href="enquiry.php">enquiry</a></span> <span class="lnr lnr-database"></span> <span class="likes"><a href="log.php?GET=rules">Rules</a></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End events Area -->

        <!-- Start speaker Area -->
        <section id="about" class="speaker-area section-gap">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-8 pb-80 header-text">
                        <h1>Our Organisers</h1>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> labore  et dolore magna aliqua.
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 speaker-wrap">
                        <div class="single-speaker">
                            <div class="content">
                                <a href="#" target="_blank">
                                    <div class="content-overlay"></div>
                                    <img class="content-image img-fluid d-block mx-auto" style="height: 152px; width: 100%;" src="img/s1.jpg" alt="">
                                    <div class="content-details fadeIn-bottom"></div>
                                </a>
                            </div>
                        </div>
                        <h2>Presented By</h2>
                        <p>Bhagwan Mahavir Education Foundation</p>
                    </div>
                    <div class="col-lg-4 col-md-4 speaker-wrap">
                        <div class="single-speaker">
                            <div class="content">
                                <a href="#" target="_blank">
                                    <div class="content-overlay"></div>
                                    <img class="content-image img-fluid d-block mx-auto"  style="height: 152px; width: 100%;"src="img/s2.jpg" alt="">
                                    <div class="content-details fadeIn-bottom"></div>
                                </a>
                            </div>
                        </div>
                        <h2>Sponsored By</h2>
                        <p>Célèbre Aesthetics </p>
                    </div>
                    <div class="col-lg-4 col-md-4 speaker-wrap">
                        <div class="single-speaker">
                            <div class="content">
                                <a href="#" target="_blank">
                                    <div class="content-overlay"></div>
                                    <img class="content-image img-fluid d-block mx-auto" style="height: 152px; width: 100%;" src="img/s3.png" alt="">
                                    <div class="content-details fadeIn-bottom"></div>
                                </a>
                            </div>
                        </div>
                        <h2>Sponsored By</h2>
                        <p>Gateforum</p>
                    </div>

                </div>
                
                <div class="row">
                    <div class="col-lg-4 col-md-4 speaker-wrap">
                        <div class="single-speaker">
                            <div class="content">
                                <a href="#" target="_blank">
                                    <div class="content-overlay"></div>
                                    <img class="content-image img-fluid d-block mx-auto" style="height: 152px; width: 100%;" src="img/s4.png" alt="">
                                    <div class="content-details fadeIn-bottom"></div>
                                </a>
                            </div>
                        </div>
                        <h2>Sponsored By</h2>
                        <p>Want to be a sponsor contact 814-165-2305</p>
                    </div>
                    <div class="col-lg-4 col-md-4 speaker-wrap">
                        <div class="single-speaker">
                            <div class="content">
                                <a href="#" target="_blank">
                                    <div class="content-overlay"></div>
                                    <img class="content-image img-fluid d-block mx-auto"  style="height: 152px; width: 100%;"src="img/s5.png" alt="">
                                    <div class="content-details fadeIn-bottom"></div>
                                </a>
                            </div>
                        </div>
                        <h2>Sponsored By</h2>
                        <p>Want to be a sponsor contact 814-165-2305</p>
                    </div>
                    <div class="col-lg-4 col-md-4 speaker-wrap">
                        <div class="single-speaker">
                            <div class="content">
                                <a href="#" target="_blank">
                                    <div class="content-overlay"></div>
                                    <img class="content-image img-fluid d-block mx-auto" style="height: 152px; width: 100%;" src="img/s6.png" alt="">
                                    <div class="content-details fadeIn-bottom"></div>
                                </a>
                            </div>
                        </div>
                        <h2>Sponsored By</h2>
                        <p>Want to be a sponsor contact 814-165-2305</p>
                    </div>

                </div>
        </section>
        <!-- End speaker Area -->


        <!-- start footer Area -->
        <footer class="footer-area section-gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6  col-md-12">
                        <div class="single-footer-widget newsletter">
                            <h6>Newsletter</h6>
                            <p>You can trust us. we only send promo offers, not a single spam.</p>
                            <div>
                                <form id="form"  action="subs.php" class="form-inline">
                                    <div class="form-group row" style="width: 100%">
                                        <div class="col-lg-8 col-md-12">
                                            <input name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">
                                        </div>
                                        <div class="col-lg-4 col-md-12">
                                            <button class="nw-btn primary-btn" type="submit" >Subscribe<span class="lnr lnr-arrow-right"></span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1  col-md-12"></div>
                    <div class="col-lg-5  col-md-12">
                        <div class="single-footer-widget mail-chimp">
                            <h6 class="mb-20">Last Year Memories</h6>
                            <ul class="d-flex flex-wrap ">
                                <li><img style="height: 58px;width: 58px;" src="img/i1.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i2.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i3.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i4.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i5.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i6.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i7.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i8.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i1.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i2.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i3.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i4.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i5.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i6.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i7.jpg" alt=""></li>
                                <li><img style="height: 58px;width: 58px;" src="img/i8.jpg" alt=""></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End footer Area -->

        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/availability-calendar.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/parallax.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/main.js"></script>
        <script src="js/baguetteBox.min.js"></script>
        <script type="text/javascript">
                                                baguetteBox.run('.tz-gallery');
        </script>
    </body>
</html>
